var express = require('express'),
    fs      = require('fs'),
    path = require('path'),
    fsx = require('fs-extra'),
    caminho = path.join(__dirname, '..\\content\\'),
    pathStyle = path.join(__dirname, '..\\projects\\'),
    date = new Date(),
    filecompile = [];

    var Config = function () {};


    Config.prototype.readFile = function (file){
        return JSON.parse(fs.readFileSync(caminho+file, 'utf8'));
    };


    Config.prototype.readConfig = function(type,project) {
        var mod = null, 
            result, key; 
        fs.readdirSync(caminho).forEach(function(file) {
            var obj; 
            if (/\.json$/.test(file)) {
                var json = JSON.parse(fs.readFileSync(caminho+file, 'utf8'));
                json.forEach(function(name){
                    proj = name[0];
                    for(var i =0; i< json.length;i++){
                        result = json[i][1].default
                        if(proj  === project){
                            for( key in json[i][1].default){
                                if(type ===  key){
                                    mod = result[key];
                                }
                            }
                        }
                    }
                });
            }    
        });
        
        return mod;
    };

    Config.prototype.extend  =  function(destination, source) {
        var toString = Object.prototype.toString,
            objTest = toString.call({});
        for (var property in source) {
            if (source[property] && objTest == toString.call(source[property])) {
                destination[property] = destination[property] || {};
                extend(destination[property], source[property]);
            } else {
                destination[property] = source[property];
            }
        }
        return destination;
    };


    Config.prototype.CheckModule = function(arg1){
        var obj, cont, result, key,
            filecompile = [],
            project = arg1;

        fs.readdirSync(caminho).forEach(function(file) {
            if (/\.json$/.test(file)) {
                var obj = Config.prototype.readFile(file);
                
                for(var i =0; i< obj.length;i++){
                    if(obj[i][0].toLowerCase() === project.toLowerCase()){
                        result = obj[i][1].module;
                        //console.log(result)
                        for( key in obj[i][1].module ){
                            if (result[key]) {
                                //console.log(key)
                                if(fs.existsSync(caminho+'/modules/MODULE_'+key+'.js')){

                                    filecompile.push(key +'.js');
                                
                                }
                            }
                        }
                    }
                }
            }

        });

        return  function(){
            try {
                if (filecompile.length === 0) {
                    throw "this project has not setted modules for it!";
                }else{

                    return filecompile ;
                }
                //return true;
            } catch (e) {
                var design =   '****************************\n'+
                                e +'\n\n'+
                                '********* WARNING **********\n';

                console.error(design);

                return false;
            }

        };
    };


    Config.prototype.checkLibs = function(project) {
        var cont, 
            libsCompile = [], 
            count,f, result, key;

        fs.readdirSync(caminho).forEach(function(file) {
            if (/\.json$/.test(file)) {
                var obj = Config.prototype.readFile(file);
                for(var i =0; i< obj.length;i++){
                    if(obj[i][0].toLowerCase() === project.toLowerCase()){
                        result = obj[i][1].module;
                        count = 0;
                        for( key in obj[i][1].module ){
                            if (result[key]) {
                                var  newName = count < 10  ?  "0"+count+key : count+key;
                                if(fs.existsSync(caminho+'/modules/MODULE_'+key+'.js')){

                                    libsCompile.push(newName +'.js');
                                    count++;
                                }
                            }
                        }
                    }
                }                
            }
        });
        return libsCompile;
    };
    Config.prototype.checkStyles = function(project,styles){
        var foldercss = styles,
            listCss = [],
            index =0;

        fs.readdirSync(caminho).forEach(function(file) {
            var obj, total, result,
                key,f,
                cont,name, set, 
                count, nameFile;

            if (/\.json$/.test(file)) {
               // f = grunt.file.readJSON(contentPath+file);
               // cont = JSON.parse(JSON.stringify(f));
                var cont = Config.prototype.readFile(file);
                for(var i =0; i< cont.length;i++){
                    if (cont[i][0].toLowerCase() === project.toLowerCase()) {
                        obj = cont[i];
                        result = cont[i][1].styles;
                        for( name in cont[i][1].styles ){
                            set = result[name];
                            if(name === foldercss){
                                count = 0;
                                for(key in set ){
                                    if (set[key]) {
                                        nameFile = key+'.css';
                                        var  newName = count < 10  ?  "0"+count+key+'.css': count+key+'.css';
                                        if(fs.existsSync(pathStyle+project+'/styles/'+foldercss+'/'+nameFile)){
                                            listCss.push(newName);
                                            count++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        });

        return  listCss;
    };


exports.Config = Config;