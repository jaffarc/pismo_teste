#!/bin/env node
//  OpenShift sample Node application
var express = require('express');
var fs      = require('fs');
var app     = express(),
    bodyParser  = require('body-parser');



var port = process.env.PORT || 80;
var ip   = process.env.IP   || '0.0.0.0';

var allowCrossDomain = function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');

    if ('OPTIONS' == req.method) {
      res.send(200);
    }
    else {
      next();
    }
};



app.use(bodyParser.json()); 
app.use(bodyParser.urlencoded({ extended: true })); 
app.use(allowCrossDomain);
app.set('etag', 'weak');


app.configure(function() {
    app.use(express.static(__dirname + '/www'));
});


app.get('/', function(req, res) {
  res.sendfile(__dirname + '/agenda/index.html');
});


app.get('/api/users', function(req, res) {
    
    res.json({ user: 'tobi' })
});


app.post('/api/users', function(req, res) {
    
    console.log(req.body)
    
    res.json({ user: req.body })
   
});

app.put('/api/users', function(req, res) {
    
    console.log(req.body)
    
    res.json({ user: req.body})
   
});



app.listen(port, ip);
