/* jshint undef: true, unused: true */
/* globals APP */

/**
 * [description]
 * @param  {[type]} 
 * @return {[type]} [description]
 */
APP.namespace(['http'], function (app) {

    /*if(!APP.startView('[data-a="1"]')){
        return false
    }
	*/
	console.log(app)
	
	var template = APP.tpl.create(document.getElementById('target'));
	template.scope.name = "jaffar cardoso";
	//template.scope.age = app.age();

	template.scope.link = "www.jaffar.com.br";

	template.scope.greet = "Hello";
	template.scope.items = [
		{ "name": "jaffar" },
		{ "name": "front-end" },
		{ "name": "developer" }
	];
   

    template.render();
	
	
    var callback = {
      success : function(data){
         console.log(1, 'success', JSON.parse(data));
         template.scope.items = [JSON.parse(data)];
          template.render();
      },
      error : function(data){
         console.log(2, 'error', JSON.parse(data));
      }
    };

  
	var uri = 'http://nodejs-jaffarc.rhcloud.com/api/users';
    var payload = {
      'topic' : 'js',
      'q'     : 'Promise'
    };
    

	app.$http({
	  	method: 'GET',
	  	url : uri,
		params: payload,
	    headers: {
	       'Content-Type': 'application/json;charset=utf-8'

	    }
	})
	.then(callback.success)
	.catch(callback.error)

    function  showPost(){
		app.$http({
		  method: 'POST',
		  url : uri,
		    params: payload,
			    headers: {
			      'Content-Type': 'application/json'

			    }
		})
		.then(callback.success)
		.catch(callback.error)
      	
    };


	function addEvent(element, evnt, funct){
	    if (element.attachEvent)
	        return element.attachEvent('on'+evnt, funct);
	    else
	        return element.addEventListener(evnt, funct, false);
	}

	// example
	addEvent(
	    document.getElementById('myElement'),
	    'click',
	    function () {
	    	showPost(); 
	 	}
	);

});
